/* Copyright (c) 2018-2021 AlaskanEmily
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cinnamon.h"
#include "cin_openal.h"

#include <stddef.h>
#include <assert.h>

/*****************************************************************************/
/* See gen_float_table.py for how this table was generated. */
static const float char_to_float_table[0x100] = {
    0.0000000000f, 0.0000000000f, 0.0000000000f, 0.0000000000f,
    0.0000000000f, 0.0000000000f, 0.0000000000f, 0.0000000000f,
    0.0000000000f, 0.0000000000f, 0.0000000000f, 0.0000000000f,
    0.0000000000f, 0.0000000000f, 0.0000000000f, 0.0000000000f,
    0.0000000000f, 0.0094771457f, 0.0186795915f, 0.0276100044f,
    0.0362710514f, 0.0446653996f, 0.0527957157f, 0.0606646670f,
    0.0682749203f, 0.0756291427f, 0.0827300011f, 0.0895801626f,
    0.0961822940f, 0.1025390625f, 0.1086531350f, 0.1145271784f,
    0.1201638598f, 0.1255658462f, 0.1307358045f, 0.1356764018f,
    0.1403903050f, 0.1448801811f, 0.1491486971f, 0.1531985200f,
    0.1570323168f, 0.1606527545f, 0.1640625000f, 0.1672642204f,
    0.1702605826f, 0.1730542537f, 0.1756479005f, 0.1780441902f,
    0.1802457897f, 0.1822553660f, 0.1840755860f, 0.1857091168f,
    0.1871586254f, 0.1884267787f, 0.1895162437f, 0.1904296875f,
    0.1911697770f, 0.1917391791f, 0.1921405610f, 0.1923765895f,
    0.1924499317f, 0.1923632546f, 0.1921192251f, 0.1917205102f,
    0.1911697770f, 0.1904696923f, 0.1896229233f, 0.1886321369f,
    0.1875000000f, 0.1862291797f, 0.1848223430f, 0.1832821568f,
    0.1816112881f, 0.1798124040f, 0.1778881714f, 0.1758412573f,
    0.1736743286f, 0.1713900525f, 0.1689910958f, 0.1664801256f,
    0.1638598088f, 0.1611328125f, 0.1583018036f, 0.1553694491f,
    0.1523384160f, 0.1492113713f, 0.1459909820f, 0.1426799151f,
    0.1392808375f, 0.1357964163f, 0.1322293184f, 0.1285822108f,
    0.1248577606f, 0.1210586346f, 0.1171875000f, 0.1132470236f,
    0.1092398726f, 0.1051687137f, 0.1010362142f, 0.0968450408f,
    0.0925978607f, 0.0882973408f, 0.0839461482f, 0.0795469497f,
    0.0751024124f, 0.0706152033f, 0.0660879893f, 0.0615234375f,
    0.0569242148f, 0.0522929883f, 0.0476324249f, 0.0429451916f,
    0.0382339554f, 0.0335013833f, 0.0287501422f, 0.0239828993f,
    0.0192023213f, 0.0144110755f, 0.0096118286f, 0.0048072478f,
    0.5000000000f, 0.5000000000f, 0.5000000000f, 0.5000000000f,
    0.5000000000f, 0.5000000000f, 0.5000000000f, 0.5000000000f,
    0.5000000000f, 0.5000000000f, 0.5000000000f, 0.5000000000f,
    0.5000000000f, 0.5000000000f, 0.5000000000f, 0.5000000000f,
    0.5000000000f, 0.5094771457f, 0.5186795915f, 0.5276100044f,
    0.5362710514f, 0.5446653996f, 0.5527957157f, 0.5606646670f,
    0.5682749203f, 0.5756291427f, 0.5827300011f, 0.5895801626f,
    0.5961822940f, 0.6025390625f, 0.6086531350f, 0.6145271784f,
    0.6201638598f, 0.6255658462f, 0.6307358045f, 0.6356764018f,
    0.6403903050f, 0.6448801811f, 0.6491486971f, 0.6531985200f,
    0.6570323168f, 0.6606527545f, 0.6640625000f, 0.6672642204f,
    0.6702605826f, 0.6730542537f, 0.6756479005f, 0.6780441902f,
    0.6802457897f, 0.6822553660f, 0.6840755860f, 0.6857091168f,
    0.6871586254f, 0.6884267787f, 0.6895162437f, 0.6904296875f,
    0.6911697770f, 0.6917391791f, 0.6921405610f, 0.6923765895f,
    0.6924499317f, 0.6923632546f, 0.6921192251f, 0.6917205102f,
    0.6911697770f, 0.6904696923f, 0.6896229233f, 0.6886321369f,
    0.6875000000f, 0.6862291797f, 0.6848223430f, 0.6832821568f,
    0.6816112881f, 0.6798124040f, 0.6778881714f, 0.6758412573f,
    0.6736743286f, 0.6713900525f, 0.6689910958f, 0.6664801256f,
    0.6638598088f, 0.6611328125f, 0.6583018036f, 0.6553694491f,
    0.6523384160f, 0.6492113713f, 0.6459909820f, 0.6426799151f,
    0.6392808375f, 0.6357964163f, 0.6322293184f, 0.6285822108f,
    0.6248577606f, 0.6210586346f, 0.6171875000f, 0.6132470236f,
    0.6092398726f, 0.6051687137f, 0.6010362142f, 0.5968450408f,
    0.5925978607f, 0.5882973408f, 0.5839461482f, 0.5795469497f,
    0.5751024124f, 0.5706152033f, 0.5660879893f, 0.5615234375f,
    0.5569242148f, 0.5522929883f, 0.5476324249f, 0.5429451916f,
    0.5382339554f, 0.5335013833f, 0.5287501422f, 0.5239828993f,
    0.5192023213f, 0.5144110755f, 0.5096118286f, 0.5048072478f,
    1.0000000000f, 1.0000000000f, 1.0000000000f, 1.0000000000f,
    1.0000000000f, 1.0000000000f, 1.0000000000f, 1.0000000000f,
    1.0000000000f, 1.0000000000f, 1.0000000000f, 1.0000000000f,
    1.0000000000f, 1.0000000000f, 1.0000000000f, 1.0000000000f
};

/*****************************************************************************/

CIN_PRIVATE_PURE(ALuint)
    Cin_CinFormatToOpenALFormat(enum Cin_Format f, unsigned num_channels){
    
    switch(f){
        case Cin_eFormatS8:
            if(num_channels == 1)
                return AL_FORMAT_MONO8;
            if(num_channels == 2)
                return AL_FORMAT_STEREO8;
            if(num_channels == 4)
                return AL_FORMAT_QUAD8;
            assert(0);
            return 0;
        case Cin_eFormatS16:
            if(num_channels == 1)
                return AL_FORMAT_MONO16;
            if(num_channels == 2)
                return AL_FORMAT_STEREO16;
            if(num_channels == 4)
                return AL_FORMAT_QUAD16;
            assert(0);
            return 0;
        case Cin_eFormatS32:
            assert(0);
            return 0;
        case Cin_eFormatFloat32:
            if(num_channels == 1)
                return AL_FORMAT_MONO_FLOAT32;
            if(num_channels == 2)
                return AL_FORMAT_STEREO_FLOAT32;
            assert(0);
            return 0;
        case Cin_eFormatFloat64:
            if(num_channels == 1)
                return AL_FORMAT_MONO_DOUBLE_EXT;
            if(num_channels == 2)
                return AL_FORMAT_STEREO_DOUBLE_EXT;
            assert(0);
            return 0;
        case Cin_eFormatULaw8:
            if(num_channels == 1)
                return AL_FORMAT_MONO_MULAW_EXT;
            if(num_channels == 2)
                return AL_FORMAT_STEREO_MULAW_EXT;
            if(num_channels == 4)
                return AL_FORMAT_QUAD_MULAW;
            assert(0);
            return 0;
    }
    assert(0);
    return 0;
}

/*****************************************************************************/

CIN_PRIVATE(int) Cin_CinExtension(const struct Cin_Driver *drv, int ext){
    if((drv->ext & 1) == 0){
        short s = 0;
        alcMakeContextCurrent(drv->ctx);
        if(alIsExtensionPresent("AL_EXT_float32"))
            s |= (1 << CIN_FLOAT_SUPPORTED);
        
        if(alIsExtensionPresent("AL_EXT_double"))
            s |= (1 << CIN_DOUBLE_SUPPORTED);
        
        if(alIsExtensionPresent("AL_EXT_MULAW") ||
            alIsExtensionPresent("AL_EXT_mulaw") ||
            alIsExtensionPresent("AL_EXT_ULAW") ||
            alIsExtensionPresent("AL_EXT_ulaw") )
            s |= (1 << CIN_MULAW_SUPPORTED);
        
        if(alIsExtensionPresent("AL_EXT_MCFORMATS") ||
            alIsExtensionPresent("AL_EXT_mcformats"))
            s |= (1 << CIN_QUAD_SUPPORTED);
        
        if(alIsExtensionPresent("AL_EXT_MULAW_MCFORMATS") ||
            alIsExtensionPresent("AL_EXT_mulaw_mcformats") ||
            alIsExtensionPresent("AL_EXT_ULAW_MCFORMATS") ||
            alIsExtensionPresent("AL_EXT_ulaw_mcformats"))
            s |= (1 << CIN_MULAW_QUAD_SUPPORTED);
        
        ((struct Cin_Driver *)drv)->ext = s;
        
        return s & (1 << ext);
    }
    
    return drv->ext & (1 << ext);
}

/*****************************************************************************/

CIN_PRIVATE(int) Cin_CleanOpenALSound(ALuint snd, ALuint *out){
    ALint i;
    alGetSourcei(snd, AL_BUFFERS_PROCESSED, &i);
    if(i > 0){
        ALuint buffers[16];
        if(out != NULL){
            --i;
            alSourceUnqueueBuffers(snd, 1, out);
        }
        do{
            const unsigned to_delete = (i >= 16) ? 16 : i;
            alSourceUnqueueBuffers(snd, to_delete, buffers);
            alDeleteBuffers(to_delete, buffers);
            i -= to_delete;
        }while(i > 0);
        return 1;
    } /* if(i > 0) */
    return 0;
}

/*****************************************************************************/

unsigned Cin_StructDriverSize(){
    return sizeof(struct Cin_Driver);
}

/*****************************************************************************/

enum Cin_DriverError Cin_CreateDriver(struct Cin_Driver *drv){
    ALCcontext *ctx;
    ALCdevice *dev;
    
    assert(drv != NULL);
    
    dev = alcOpenDevice(NULL);
    if(dev == NULL){
        return Cin_eDriverNoDevice;
    }
    
    ctx = alcCreateContext(dev, NULL);
    
    if(ctx == NULL){
        alcCloseDevice(dev);
        return Cin_eDriverFailure;
    }
    
    drv->ctx = ctx;
    drv->dev = dev;
    drv->ext = 0;
    
    return Cin_eDriverSuccess;
}

/*****************************************************************************/

void Cin_DestroyDriver(struct Cin_Driver *drv){
    assert(drv != NULL);
    assert(drv->ctx != NULL);
    assert(drv->dev != NULL);
    
    alcDestroyContext(drv->ctx);
    alcCloseDevice(drv->dev);
}

/*****************************************************************************/

enum Cin_DriverError Cin_DriverSupportsFormat(
    const struct Cin_Driver *drv,
    enum Cin_Format format,
    unsigned num_channels){
    
    assert(drv != NULL);
    assert(drv->ctx != NULL);
    assert(drv->dev != NULL);
    
    switch(format){
        case Cin_eFormatS16: /* FALLTHROUGH */
        case Cin_eFormatS8:
            if(num_channels == 1 || num_channels == 2){
                return Cin_eDriverSuccess;
            }
            else if(num_channels == 4){
                return Cin_CinExtension(drv, CIN_QUAD_SUPPORTED) ?
                    Cin_eDriverSuccess : Cin_eDriverUnsupportedNumChannels;
            }
            else{
                return Cin_eDriverUnsupportedNumChannels;
            }
        case Cin_eFormatS32:
            return Cin_eDriverUnsupportedFormat;
        case Cin_eFormatFloat32:
            if(num_channels == 1 || num_channels == 2){
                return Cin_CinExtension(drv, CIN_FLOAT_SUPPORTED) ?
                    Cin_eDriverSuccess : Cin_eDriverUnsupportedFormat;
            }
            else{
                return Cin_eDriverUnsupportedNumChannels;
            }
        case Cin_eFormatFloat64:
            if(num_channels == 1 || num_channels == 2){
                return Cin_CinExtension(drv, CIN_DOUBLE_SUPPORTED) ?
                    Cin_eDriverSuccess : Cin_eDriverUnsupportedFormat;
            }
            else{
                return Cin_eDriverUnsupportedNumChannels;
            }
        case Cin_eFormatULaw8:
            if(!Cin_CinExtension(drv, CIN_MULAW_SUPPORTED))
                return Cin_eDriverUnsupportedFormat;
            if(num_channels == 1 || num_channels == 2){
                return Cin_eDriverSuccess;
            }
            else if(num_channels == 4){
                return Cin_CinExtension(drv, CIN_MULAW_QUAD_SUPPORTED) ?
                    Cin_eDriverSuccess : Cin_eDriverUnsupportedNumChannels;
            }
            else{
                return Cin_eDriverUnsupportedNumChannels;
            }
        case Cin_eFormatNUM_FORMATS:
            return Cin_eDriverInvalidFormat;
    }
    
    return Cin_eDriverInvalidFormat;
}

/*****************************************************************************/

enum Cin_DriverError Cin_DriverSupportsSampleRate(
    const struct Cin_Driver *drv,
    unsigned rate){
    
    assert(drv != NULL);
    assert(drv->ctx != NULL);
    assert(drv->dev != NULL);
    
    switch(rate){
        case 8000: /* FALLTHROUGH */
        case 11025: /* FALLTHROUGH */
        case 16000: /* FALLTHROUGH */
        case 22050: /* FALLTHROUGH */
        case 44100: /* FALLTHROUGH */
        case 48000:
            return Cin_eDriverSuccess;
        default:
            return Cin_eDriverUnsupportedSampleRate;
    
    }
}

/*****************************************************************************/

unsigned Cin_StructLoaderSize(){
    return sizeof(struct Cin_Loader);
}

/*****************************************************************************/

enum Cin_LoaderError Cin_CreateLoader(struct Cin_Loader *out,
    struct Cin_Driver *drv,
    unsigned sample_rate,
    unsigned num_channels,
    enum Cin_Format format){
    
    assert(out);
    assert(drv);
    assert(drv->ctx);
    assert(drv->dev);
#if 0
    {
        const enum Cin_LoaderError err =
            Cin_FormatCompatible(drv, sample_rate, num_channels, format);
        if(err != Cin_eLoaderSuccess)
            return err;
    }
#endif
    
    alcMakeContextCurrent(drv->ctx);
    
    out->snd.ctx = drv->ctx;
    
    {
        ALuint source;
        alGenSources(1, &source);
        out->snd.snd = source;
        
        alSourcef(source, AL_PITCH, 1.0f);
        alSourcef(source, AL_GAIN, 1.0f);
        alSource3f(source, AL_POSITION, 0.0f, 0.0f, 0.0f);
        alSource3f(source, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
        alSourcei(source, AL_LOOPING, AL_FALSE);
    }
    
    out->format = Cin_CinFormatToOpenALFormat(format, num_channels);
    out->channels = num_channels;
    out->sample_rate = sample_rate;
    
    return Cin_eLoaderSuccess;
}

/*****************************************************************************/

enum Cin_LoaderError Cin_LoaderPut(struct Cin_Loader *ld,
    const void *data,
    unsigned byte_size){
    
    ALuint buffer;
    alcMakeContextCurrent(ld->snd.ctx);
    if(!Cin_CleanOpenALSound(ld->snd.snd, &buffer)){
        alGenBuffers(1, &buffer);
    }
    alBufferData(buffer, ld->format, data, byte_size, ld->sample_rate);
    alSourceQueueBuffers(ld->snd.snd, 1, &buffer);
    
    return Cin_eLoaderSuccess;
}

/*****************************************************************************/

enum Cin_LoaderError Cin_LoaderFinalize(struct Cin_Loader *ld,
    struct Cin_Sound *out){
    
    ALuint buffer;
    alcMakeContextCurrent(ld->snd.ctx);
    if(Cin_CleanOpenALSound(ld->snd.snd, &buffer)){
        alDeleteBuffers(1, &buffer);
    }
    
    out->snd = ld->snd.snd;
    out->ctx = ld->snd.ctx;
    out->volume = 0xFF; /* Default volume is 100% */
    
    ld->snd.snd = 0;
    return Cin_eLoaderSuccess;
}

/*****************************************************************************/

unsigned Cin_StructSoundSize(){
    return sizeof(struct Cin_Sound);
}

/*****************************************************************************/

enum Cin_SoundError Cin_SoundPlay(struct Cin_Sound *snd){
    alcMakeContextCurrent(snd->ctx);
    alSourcei(snd->snd, AL_LOOPING, AL_FALSE);
    alSourcePlay(snd->snd);
    return Cin_eSoundSuccess;
}

/*****************************************************************************/

enum Cin_SoundError Cin_SoundPlayLoop(struct Cin_Sound *snd, int loop){
    alcMakeContextCurrent(snd->ctx);
    alSourcei(snd->snd, AL_LOOPING, (loop==0) ? AL_FALSE : AL_TRUE);
    alSourcePlay(snd->snd);
    return Cin_eSoundSuccess;
}

/*****************************************************************************/

enum Cin_SoundError Cin_SoundStop(struct Cin_Sound *snd){
    alcMakeContextCurrent(snd->ctx);
    alSourceStop(snd->snd);
    alSourceRewind(snd->snd);
    return Cin_eSoundSuccess;
}

/*****************************************************************************/

enum Cin_SoundError Cin_SoundSetVolume(struct Cin_Sound *snd,
    unsigned char volume){
    
    const float gain = char_to_float_table[volume];
    
    assert(snd != NULL);
    
    /* Store the input volume as-is to be sure that calls to
     * Cin_SoundSetVolume/Cin_SoundGetVolume are round-trip safe.
     */
    snd->volume = volume;
    
    alSourcef(snd->snd, AL_GAIN, gain);
    if(alGetError() != AL_NO_ERROR)
        return Cin_eSoundFailure;
    else
        return Cin_eSoundSuccess;
}

/*****************************************************************************/

enum Cin_SoundError Cin_SoundGetVolume(const struct Cin_Sound *snd,
    unsigned char *out_volume){
    
    assert(snd != NULL);
    assert(out_volume != NULL);
    
    out_volume[0] = snd->volume;
    return Cin_eSoundSuccess;
}

/*****************************************************************************/

void Cin_DestroySound(struct Cin_Sound *snd){
    alcMakeContextCurrent(snd->ctx);
    alDeleteSources(1, &(snd->snd));
}
