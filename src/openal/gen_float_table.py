#  Generates the C table of floats used translate from 0u-255u to 0.0f to 1.0f
#
#  Copyright (C) 2021 AlaskanEmily
#
#  This software is provided 'as-is', without any express or implied
#  warranty.  In no event will the authors be held liable for any damages
#  arising from the use of this software.
#
#  Permission is granted to anyone to use this software for any purpose,
#  including commercial applications, and to alter it and redistribute it
#  freely, subject to the following restrictions:
#
#  - The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
#  - Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
#  - This notice may not be removed or altered from any source distribution.
#

LOW_SNAP = 16
MID_SNAP0 = (128 - 8)
MID_SNAP1 = (128 + 8)
HI_SNAP = 240

LOW_LERP_TANGENT = 0.0
MID_LERP_TANGENT = 1.0
HIGH_LERP_TANGENT = 0.5

def CubicInterp(ValT, LoT, HiT, LoV, HiV):
    # Convert T to 0.0-1.0
    T = float(ValT - LoT) / float(HiT - LoT)
    A = (2.0 * pow(T, 3)) - (3.0 * pow(T, 2)) + 1.0
    B = pow(T, 3) - (2.0 * pow(T, 2)) + T 
    C = (-2.0 * pow(T, 3)) + (3.0 * pow(T, 2)) + 0.0
    D = pow(T, 3) - (2.0 * pow(T, 2))
    if LoT < 0.5:
        Tan0 = LOW_LERP_TANGENT
        Tan1 = MID_LERP_TANGENT
    else:
        Tan0 = MID_LERP_TANGENT
        Tan1 = HIGH_LERP_TANGENT
    
    return (A * LoV) + (B * Tan0) + (C * HiV) + (D * Tan1)

def Lerp(T, LoT, HiT, LoV, HiV):
    return LoV + ((float(T - LoT) / float(HiT - LoT)) * (HiV - LoV))

def DoAppend():
    Line = "    "
    i = 0
    while True:
        Value = (yield)
        if Value == '\n':
            i = 0
        else:
            Str = "%1.10Ff, " % Value
            Line += Str
            i += 1
        
        if i % 4 == 0:
            print(Line)
            Line = "    "

print("{")

Append = DoAppend()
next(Append)

e = 0
while e < LOW_SNAP:
    Append.send(0.0)
    e += 1

while e < MID_SNAP0:
    Value = CubicInterp(e, LOW_SNAP, MID_SNAP0, 0.0, 0.5)
    Append.send(Value)
    e += 1

while e < MID_SNAP1:    
    Append.send(0.5)
    e += 1

while e < HI_SNAP:
    Value = CubicInterp(e, MID_SNAP1, HI_SNAP, 0.5, 1.0)
    Append.send(Value)
    e += 1

while e <= 255:
    Append.send(1.0)
    e += 1

Append.send('\n')
print("};")
