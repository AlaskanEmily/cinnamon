// Copyright (c) 2018-2021 AlaskanEmily
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "cin_dsound_driver.hpp"
#include "cin_dsound_sound.hpp"
#include "cin_soft_loader.h"
#include "cinnamon.h"

#include <assert.h>
#include <new>

///////////////////////////////////////////////////////////////////////////////

CIN_PRIVATE(bool) cin_supports_format(IDirectSound8 *dsound,
    unsigned rate,
    unsigned channels,
    enum Cin_Format sample_type){

    WAVEFORMATEXTENSIBLE fmt;
    DSBUFFERDESC buffer_desc;
    Cin_Sound::CreateWaveFormat(rate, channels, sample_type, fmt);
    
    buffer_desc.dwSize = sizeof(DSBUFFERDESC);
    buffer_desc.dwFlags = DSBCAPS_GLOBALFOCUS;
    // Arbitrary, medium-sized power-of-two size
    buffer_desc.dwBufferBytes = 4096;
    buffer_desc.dwReserved = 0;
    buffer_desc.lpwfxFormat = &fmt.Format;
    buffer_desc.guid3DAlgorithm = DS3DALG_DEFAULT;
    
    IDirectSoundBuffer *buffer;
    if(dsound->CreateSoundBuffer(&buffer_desc, &buffer, NULL) != DS_OK){
        return false;
    }
    else{
        buffer->Release();
        return true;
    }
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(unsigned) Cin_StructDriverSize(void){
    return sizeof(Cin_Driver);
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_DriverError) Cin_CreateDriver(struct Cin_Driver *drv){
    new (drv) Cin_Driver();
    return Cin_eDriverSuccess;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(void) Cin_DestroyDriver(struct Cin_Driver *drv){
    drv->~Cin_Driver();
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_DriverError) Cin_DriverSupportsFormat(
    const struct Cin_Driver *drv,
    enum Cin_Format format,
    unsigned num_channels){
    
    if(drv == NULL)
        return Cin_eDriverFailure;
    
    if(IDirectSound8 *const dsound =
        const_cast<Cin_Driver*>(drv)->getDirectSound()){
        
        // Try to create a buffer with this format and num channels, at
        // either 44100 or 48000 Hz.
        if(cin_supports_format(dsound, 44100, num_channels, format) ||
            cin_supports_format(dsound, 48000, num_channels, format)){
            
            return Cin_eDriverSuccess;
        }
        else{
            return Cin_eDriverFailure;
        }
    }
    else{
        return Cin_eDriverFailure;
    }
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_DriverError) Cin_DriverSupportsSampleRate(
    const struct Cin_Driver *drv,
    unsigned rate){
    
    if(drv == NULL)
        return Cin_eDriverFailure;
    
    if(IDirectSound8 *const dsound =
        const_cast<Cin_Driver*>(drv)->getDirectSound()){
        
        // Try to create a buffer with this rate and basic setup
        if(cin_supports_format(dsound, rate, 2, Cin_eFormatS16) ||
            cin_supports_format(dsound, rate, 2, Cin_eFormatFloat32)){
            
            return Cin_eDriverSuccess;
        }
        else{
            return Cin_eDriverFailure;
        }
    }
    else{
        return Cin_eDriverFailure;
    }
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(unsigned) Cin_StructSoundSize(){
    return sizeof(Cin_Sound);
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_SoundError) Cin_SoundPlay(Cin_Sound *snd){
    snd->play(false);
    return Cin_eSoundSuccess;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_SoundError) Cin_SoundPlayLoop(Cin_Sound *snd, int loop){
    snd->play(!!loop);
    return Cin_eSoundSuccess;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_SoundError) Cin_SoundStop(Cin_Sound *snd){
    snd->stop();
    return Cin_eSoundSuccess;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_SoundError) Cin_SoundSetVolume(struct Cin_Sound *snd,
    unsigned char volume){
    
    assert(snd != NULL);
    if(snd->setVolume(volume))
        return Cin_eSoundSuccess;
    else
        return Cin_eSoundFailure;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_SoundError) Cin_SoundGetVolume(const struct Cin_Sound *snd,
    unsigned char *out_volume){
    
    assert(snd != NULL);
    assert(out_volume != NULL);
    
    unsigned char volume;
    if(snd->getVolume(volume)){
        out_volume[0] = volume;
        return Cin_eSoundSuccess;
    }
    else{
        return Cin_eSoundFailure;
    }
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(void) Cin_DestroySound(Cin_Sound *snd){
    snd->~Cin_Sound();
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_LoaderError) Cin_CreateLoader(Cin_Loader *out,
    Cin_Driver *drv,
    unsigned sample_rate,
    unsigned channels,
    enum Cin_Format format){
    Cin_CreateSoftLoader(out, sample_rate, channels, format);
    out->data = drv;
    return Cin_eLoaderSuccess;
}

///////////////////////////////////////////////////////////////////////////////

CIN_EXPORT(enum Cin_LoaderError) Cin_LoaderFinalize(Cin_Loader *ld, Cin_Sound *out){
    Cin_Driver *drv = static_cast<Cin_Driver*>(ld->data);
    drv->createSound(out, *ld);
    Cin_LoaderFreeData(ld->first);
    return Cin_eLoaderSuccess;
}
